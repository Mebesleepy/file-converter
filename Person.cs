﻿using System.Collections.Generic;

namespace read_and_convert_ICOM
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Telephone Telephone;

        public Address Address;

        public List<FamilyMember> Family = new List<FamilyMember>();

        public bool ShouldSerializeAddress()
        {
            return Address != null;
        }

        public bool ShouldSerializeTelephone()
        {
            return Telephone != null;
        }

        public bool ShouldSerializeFirstName()
        {
            return FirstName != null;
        }

       public bool ShouldSerializeLastname()
        {
            return LastName != null;
        }

      public bool ShouldSerializeFamilyMembers()
        {
            return Family != null;
        }
    }
}
