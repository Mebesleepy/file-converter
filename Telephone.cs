﻿namespace read_and_convert_ICOM
{
   public class Telephone
    {
        public string MobilePhone { get; set; }
        public string FixedNumber { get; set; }

        public bool ShouldSerializeMobilePhone()
        {
            return MobilePhone != null;
        }

        public bool ShouldSerializeFixedNumber()
        {
            return FixedNumber != null;
        }
    }
}
