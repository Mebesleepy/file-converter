﻿namespace read_and_convert_ICOM
{
   public class Address
    {
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }

        public bool ShouldSerializeStreet()
        {
            return Street != null;
        }

        public bool ShouldSerializePostalCode()
        {
            return PostalCode != null;
        }

        public bool ShouldSerializeCity()
        {
            return City != null;
        }
    }
}
