﻿
namespace read_and_convert_ICOM
{
   public class FamilyMember
    {
        public string Name { get; set; }
        public string YearOfBirth { get; set; }

        public Telephone Telephone;

        public Address Address;

        public bool ShouldSerializeAddress()
        {
            return Address != null;
        }
        public bool ShouldSerializeTelephone()
        {
            return Telephone != null;
        }

        public bool ShouldSerializeName()
        {
            return Name != null;
        }

        public bool ShouldSerializeYearOfBirth()
        {
            return YearOfBirth != null;
        }
    }
}
