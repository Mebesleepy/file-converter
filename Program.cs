﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace read_and_convert_ICOM
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileToReadFrom = @"C:\resources\some_data.txt";
            string jsonFileLocation = @"C:\resources\some_data.json";
            string xmlFileLoocation = @"C:\resources\some_data.xml";
            string outputFormat;

            if (File.Exists(fileToReadFrom))
            {
                outputFormat = askForFormat();
                List<string[]> choppedData = readLinesFromFile(fileToReadFrom);
               List<Person> personObjects = convertDataToObjects(choppedData);


                if (outputFormat == "JSON")
                {
                    writeJsonToFile(personObjects, jsonFileLocation);
                }
                else if (outputFormat == "XML")
                {
                   writeXmlToFile(personObjects, xmlFileLoocation);
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("The file to read data from does not exist, please create {0}", fileToReadFrom);
                Console.ResetColor();
            }
        }
        private static List<Person> convertDataToObjects(List<string[]> list)
        {
            List <Person> persons = new List<Person>();
            Person person = new Person();
            FamilyMember family = new FamilyMember();
            bool firstIteration = true;
            bool addingfamily = false;
            int count = 1;

            try
            {

                foreach (string[] data in list)
                {
                    switch (data[0])
                    {
                        case "P":

                            if (addingfamily)
                            {
                                person.Family.Add(family);
                                addingfamily = false;
                            }

                            if (!firstIteration)
                            {
                                persons.Add(person);
                                person = new Person();
                            }

                            person.FirstName = data[1];
                            person.LastName = data[2];

                            firstIteration = false;
                            break;
                        case "T":

                            Telephone phone = new Telephone();

                            phone.MobilePhone = data[1];
                            phone.FixedNumber = data[2];

                            if (addingfamily)
                            {
                                family.Telephone = phone;
                            } else
                            {
                                person.Telephone = phone;
                            }

                            break;
                        case "A":

                            Address address = new Address();
                            address.Street = data[1];
                            address.City = data[2];

                            if (data.Length > 3)
                            {
                                address.PostalCode = data[3];
                            }

                            if (addingfamily)
                            {
                                family.Address = address;
                            } else
                            {
                                person.Address = address;

                            }
                            break;
                        case "F":

                            if (addingfamily)
                            {
                                person.Family.Add(family);
                                family = new FamilyMember();
                            }

                            family.Name = data[1];
                            family.YearOfBirth = data[2];

                            addingfamily = true;

                            break;
                    }
                }
            } catch (ArgumentOutOfRangeException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went wrong when implementing data from document into classes, check line {0} in the provided document", count);
                Console.ResetColor();
                Console.WriteLine(e);
            }

            persons.Add(person);

            return persons;
        }

        private static void writeJsonToFile(List<Person> personObjects,string file)
        {
            File.WriteAllText(file, JsonConvert.SerializeObject(personObjects));

            Console.WriteLine("JSON file saved to {0}", file);
        }

        private static void writeXmlToFile(List<Person> personObjects, string file)
        {
            string xmlString = ConvertObjectToXMLString(personObjects);

            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(xmlString);
            xdoc.Save(file);

            Console.WriteLine("XML file saved to {0}", file);
        }

        private static string ConvertObjectToXMLString(object classObject)
        {
            string xmlString = null;
            XmlSerializer xmlSerializer = new XmlSerializer(classObject.GetType());
            using (MemoryStream memoryStream = new MemoryStream())
            {
                xmlSerializer.Serialize(memoryStream, classObject);
                memoryStream.Position = 0;
                xmlString = new StreamReader(memoryStream).ReadToEnd();
            }
            return xmlString;
        }

        private static string askForFormat()
        {
            Console.WriteLine("What format do you want for the end result?\n1. Json\n2. XML");

            string format;

            while (true)
            {
                string choice = Console.ReadLine();

                if (choice.Equals("1"))
                {
                    format = "JSON";
                    break;
                }
                else if (choice.Equals("2"))
                {
                    format = "XML";
                    break;
                }
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\nNo valid alternative selected, try again\n");
                Console.ResetColor();
            }

            Console.Clear();
            return format;
        }
        private static List<string[]> readLinesFromFile(String file)
        {
            List<string[]> listInList = new List<string[]>();


            foreach (string line in File.ReadLines(file))
            {
                string[] splited = line.Split('|');
                listInList.Add(splited);
            }
            return listInList;
        }
    }
}
